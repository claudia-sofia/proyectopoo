/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Utileria;

import Settings.Reporte;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author Gladys
 */
public class ReporteDate {

    ArrayList<Reporte> repo;

    public ReporteDate() {
        repo = new ArrayList<>();
    }

    /**
     * @param args the command line arguments
     */
    /*/public static void main(String[] args) {
        // TODO code application logic here
        Scanner sc = new Scanner(System.in);
        EjemploProyecto ej = new EjemploProyecto();
        ej.leerArchivo();
        System.out.println(ej.estudiantes);
        ej.actualizarArchivo();

    //}

    /**
     * este metodo lee un archivo y almacena en una lista
     */
    public void leerArchivo(String codigo, String paralelo,String anio,String anitemo) {
        BufferedReader csvReader = null;
        try {
            String ruta = "src/archivos/reportes/"+codigo+"-"+paralelo+ "-"+anio+"-"+anitemo+"-R"+".csv";  //ruta del archivo que se va a leer
            csvReader = new BufferedReader(new FileReader(ruta));
             String fila =csvReader.readLine();
            while (( fila = csvReader.readLine()) != null) { //iterar en el contenido del archivo
                String[] data = fila.split(",");
                repo.add(new Reporte(data[0], data[1], data[2],data[3])); //crear objeto y agregar a lista

            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ReporteDate.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ReporteDate.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                csvReader.close();
            } catch (IOException ex) {
                Logger.getLogger(ReporteDate.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    /**
     * almacena una lista en un archivo
     */
    public void actualizarArchivo(String materia,String paralelo,String anio ,String numero) {
        FileWriter writer = null;
        try {
            String ruta = "src/reportes/"+materia+"-"+paralelo+ "-"+anio+"-"+numero+"-R"+".csv"; //ruta del archivo que se va a leer
            writer = new FileWriter(ruta);
            for (Reporte est : repo) {
                writer.write(est.getFechaDeljuego()+ ";" + est.getParticipante()+ ";" + est.getNiveMaximoAlcanzado()+";"+ est.getPremio()+System.lineSeparator());
            }
            writer.close();
        } catch (IOException ex) {
            Logger.getLogger(ReporteDate.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                writer.close();
            } catch (IOException ex) {
                Logger.getLogger(ReporteDate.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

}
